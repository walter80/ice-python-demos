python -m pip  --default-timeout=100 install zeroc-ice --user

(venv)  ✘  ~/PycharmProjects/ice-python-demos /hello  python server.py
Hello World!

(venv)  ✘  ~/PycharmProjects/ice-python-demos /hello  python client.py

    usage:
    t: send greeting as twoway
    o: send greeting as oneway
    O: send greeting as batch oneway
    d: send greeting as datagram
    D: send greeting as batch datagram
    f: flush all batch requests
    T: set a timeout
    P: set a server delay
    S: switch secure mode on/off
    s: shutdown server
    x: exit
    ?: help
    
==> t
==> 

 已存在的文件夹或 Git 仓库
 cd existing_folder
 git init
 git remote add origin git@gitee.com:walter80/ice-python-demos.git
 git add .
 git commit
 git push -u origin master
 

This demo illustrates how to invoke ordinary (twoway) operations, as
well as how to make [oneway][1], [datagram][2], [secure][3], and
[batched][4] invocations.

To run the demo, first start the server:

```
python server.py
```

In a separate window, start the client:

```
python client.py
```

To test [timeouts][5] you can use 'T' to set an invocation timeout on the
client proxy and 'P' to set a delayed response in the server to cause a
timeout.

[1]: https://doc.zeroc.com/ice/4.0/client-side-features/oneway-invocations
[2]: https://doc.zeroc.com/ice/4.0/client-side-features/datagram-invocations
[3]: https://doc.zeroc.com/ice/4.0/ice-plugins/icessl
[4]: https://doc.zeroc.com/ice/4.0/client-side-features/batched-invocations
[5]: https://doc.zeroc.com/ice/4.0/client-side-features/invocation-timeouts
